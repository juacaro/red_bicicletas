var mongoose = require('mongoose');

var Bicicleta = require('../../models/bicicleta');
var request = require('request');

var server = require('../../bin/www');
const { json } = require('express');
const bicicleta = require('../../models/bicicleta');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error: '));

        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });

    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                console.log(error);
                console.log(body);
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'Content-Type' : 'application/json'};
            var aBici = '{"code": 7, "color": "azul", "modelo": "urbana", "lat": -34.6012424, "lng": -58.3861497 }';

            request.post({
                headers: headers,
                url:    base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                console.log(error);
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("azul");
                expect(bici.ubicacion[0]).toBe(-34.6012424);
                expect(bici.ubicacion[1]).toBe(-58.3861497);
                done();
            });
        });
    });



});

/* beforeEach(() => {
    console.log('testeando…');
}); */
/*
beforeEach(function(){
    console.log('testeando…'); 
});



describe('Bicicleta API', () => {
    describe('GET Bicicletas /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34.6012424, "lng": -58.3861497 }';

            request.post({
                headers: headers,
                url:    'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });
});
*/
