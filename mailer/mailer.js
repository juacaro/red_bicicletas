const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
/*
async function main() {
    let transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        //secure: STARTTLS,
        auth: {
            user: 'elmore.sauer@ethereal.email',
            pass: 'dkTa8rSHnzgmnvWFUu'
        }
    });

    let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: "elmore.sauer@ethereal.email, info@webconsulting.com.co", // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: "<b>Hello world?</b>", // html body
    });
    
    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}
    
main().catch(console.error);
*/

/*
//funciona de manera estatica como prueba
exports.sendEmail = function(req, res){
    // Definimos el transporter
    var transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'elmore.sauer@ethereal.email',
            pass: 'dkTa8rSHnzgmnvWFUu'
        }
    });
    // Definimos el email
    var mailOptions = {
        from: 'elmore.sauer@ethereal.email',
        to: 'elmore.sauer@ethereal.email, info@webconsulting.com.co',
        subject: 'Envio desde curso node',
        text: 'Contenido del email'
    };
    // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info){
        console.log(info);
        if (error){
            console.log(error);
            res.send(500, err.message);
        } else {
            console.log("Email sent");
            //res.status(200).jsonp(req.body);
            res.status(200).jsonp({
                email: info
            });
        }
    });
};
*/
/* 
//DEvelopment local
const mailConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'elmore.sauer@ethereal.email',
        pass: 'dkTa8rSHnzgmnvWFUu'
    }
};
*/

let mailConfig;
if(process.env.NODE_ENV === 'production'){
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
}else{
    if(process.env.NODE_ENV === 'satging'){
        console.log('XXXXXXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    }else{
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd
            }
        };
    }
}

module.exports = nodemailer.createTransport(mailConfig);