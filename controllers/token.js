var Usuario = require('../models/usuario');
var Token = require('../models/token');
const usuarios = require('./usuarios');

module.exports = {
    confirmationGet: function(req, res, next){
        Token.findOne({ token: req.params.token }, function(err, token){
            if(!token){
                return res.status(404).send({ type: 'not-verified', msg: 'No se encontro un usuario con este token o el token ya expiro, solicitelo nuevamente'});                
            }
            Usuario.findById(token._userId, function(err, usuario){
                if (!usuario) {
                    return res.status(404).send({ msg: 'No se encuentra el usuario con este token'});                
                }
                if(usuario.verificado){
                    return res.redirect('/usuarios');
                }
                usuario.verificado = true;
                usuario.save(function(err){
                    if (err){
                        return res.status(500).send({ msg: err.message});
                    }
                    res.redirect('/');
                })
            })
        })
    }
}